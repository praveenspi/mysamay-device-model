import { MetadataHelper } from "@neb-sports/mysamay-common-utils";
import { DeviceType } from "../enums/device-type";

var dateFields = [
    "createdTime",
    "updatedTime"
]
export class Device {

    _id: string = null;
    deviceId: string = null;
    os: string = null;
    make: string = null;
    model: string = null;
    deviceType: DeviceType = null;
    fcmToken: string = null;
    userId: string = null;
    createdTime: Date = null;
    updatedTime: Date = null;

    constructor(data: Partial<Device>) {
        Object.keys(this).forEach(key => {
            if(key in data) {
                if(dateFields.find(dateField => dateField === key)) {
                    this[key] = new Date(data[key]);
                }
                else {
                    this[key] = data[key];
                } 
            }
            else {
                delete this[key];
            }
        });
        if (!this._id) {
            this._id = MetadataHelper.generateUUID();
        }
    }

    static collectionName = "Users_DeviceInfo";
}